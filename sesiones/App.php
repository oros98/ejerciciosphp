<?php

class App
{
    function __construct(){
        session_start();
    }

    public function login(){

        require('viewLogin.php');
    }

    public function auth()
    {
        if(isset($_POST['user'])&&!empty($_POST['user'])){

            $_SESSION['usuario']= $_POST['user'];
            header('Location: index.php?method=home');
            exit();

        }else{
            header('Location: index.php?method=login');
        }

    }

    public function home(){
        if(isset($_SESSION['deseos'])){
            $deseos = $_SESSION['deseos'];
            if(isset($_REQUEST['wish']) && !empty($_REQUEST['wish'])){
                $deseos[] = $_REQUEST['wish'];
            }
        }else{
            $deseos = [];
        }

        $_SESSION['deseos']= $deseos;

        require('viewDeseos.php');
    }
    public function close(){
        session_destroy();
        unset($_SESSION);
        header("Location: index.php?method=login");
    }
    public function delete()
    {
        $key = (integer) $_REQUEST['key'];
        $deseos = $_SESSION['deseos'];
        unset($_SESSION['deseos'][$key]);
        header("Location: index.php?method=home");
    }
    public function deleteAll(){
        $_SESSION['deseos']="";
        header("Location: index.php?method=home");
    }
}
