<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

    <?php

        $precioUnidad = 20;
        $cantidad = 5;
        $iva = 1.21;
        $precioSinIva = 20;
        $precioConIva = $precioUnidad * $iva;
        $precioTotal = $precioConIva * $cantidad;

        echo "Precio de la unidad ".$precioUnidad."</br>";
        echo "Cantidad ".$cantidad."</br>";
        echo "Iva ".$iva."</br>";
        echo "Precio Sin Iva ".$precioSinIva."</br>";
        echo "Precio con iva ". $precioConIva."</br>";
        echo "Precio Total ".$precioTotal."</br>";

    ?>

</body>
</html>
