<?php
class App
{
    function __construct(){
		//Arrancamos la session
		session_start();
    }

    public function index(){

        require('view.php');
    }

    public function toggle(){
	
        //Creamos el array para almacenar las imagenes
		$arrayImagenes=[];
		
		//Comprobamos si existe la apuesta
        if(isset($_SESSION['apuesta'])){
			//Recuperamos la apuesta
            $arrayImagenes = $_SESSION['apuesta'];
        }
		
		//Numero que deseamos apostaro quitar de la apuesta
		$numero=$_GET['number'];
		
		//Comprobamos si el numero apostado existe
		if(isset($arrayImagenes[$numero])){
		
			//Eliminamos el numero apostado
			unset($arrayImagenes[$numero]);
			
		}else{
		
			//Añadimos el numero en la posicion de dicho numero
			$arrayImagenes[$numero]=$numero;
		}
        
		//Guardamos el array en session de nuevo
		$_SESSION['apuesta']=$arrayImagenes;
        
		require("view.php");
    }
}
