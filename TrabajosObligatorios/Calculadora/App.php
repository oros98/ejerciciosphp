<?php
class App
{
    //Creamos el array de las operaciones disponibles
     var $listaOperaciones = [];

    function __construct(){
    
        //Rellenamos el array
        $this->listaOperaciones[]="Suma";
        $this->listaOperaciones[]="Resta";
        $this->listaOperaciones[]="Multiplicacion";
        $this->listaOperaciones[]="Division";
    }

    public function index(){
        //Mostramos la vista.php
        require('vista.php');
    }
    public function calcular(){
        //Creamos una variable para recoger el tipo de operacion a realizar
        $nombre="";
        
        //Usamos un ternario para rellenar la variable $nombre comprobando si la request operacion existe
        isset($_REQUEST["Operacion"]) ?
        $nombre=$_REQUEST["Operacion"] : $resultado="Error!!! Seleccione una operacion";
        
        //Con este switch escogemos que operacion realizamos
        switch ($nombre) {
            case 'Suma':
                    $resultado ="El Resultado es: ".((float) $_REQUEST["operador1"]+(float) $_REQUEST["operador2"]);
                break;

            case 'Resta':
                    $resultado =(float) $_REQUEST["operador1"]-(float)  $_REQUEST["operador2"];
                break;

            case 'Multiplicacion':
                    $resultado =(float) $_REQUEST["operador1"]*(float)  $_REQUEST["operador2"];
                break;

            case 'Division':
                    $resultado =(float) $_REQUEST["operador1"]/(float)  $_REQUEST["operador2"];
                break;

            default:
                # code...
                break;
        }

        require('vista.php');
    }
}
