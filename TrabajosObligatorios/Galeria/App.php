<?php
class App
{

    function __construct(){
        
        //Si la clave tiene valor entramos en el metodo deleteImage
        if (isset($_GET['clave']))
        {
            $this->deleteImage();
        }
        
        //Si tenemos valor en detalle entramos en el metodo mostrarDetalle
        if(isset($_GET['detalle'])){
            $this->mostrarDetalle();
        }
    }

    public function index(){

        require('view.php');
    }
    public function mostrarDetalle()
    {
        //Realizamos un redireccionamiento a viewDetails.php enviando el detalle
        header("Location: viewDetails.php?imagen=$_GET[detalle]");
    }

    //Metodo que sube la imagen
    public function subirImagen(){
        $subido = true;
        $msg = '';
        $uploadedfile_size = $_FILES['ficheroSubido']['size'];

        if ($_FILES['ficheroSubido']['size'] > 200000) {
            $msg = $msg."El archivo es mayor que 200KB, debes reduzcirlo antes de subirlo<br>";
            $subido = false;
        }

        if (!($_FILES['ficheroSubido']['type'] =="image/jpeg" || $_FILES['ficheroSubido']['type'] =="image/gif")) {
            $msg = $msg."Tu archivo tiene que ser JPG o GIF.<br>";
            $subido = false;
        }
        $file_name = $_FILES['ficheroSubido']['name'];
        $add="uploads/$file_name";
        if ($subido) {
            if (move_uploaded_file($_FILES['ficheroSubido']['tmp_name'], $add)) {
                $msg= " Ha sido subido satisfactoriamente<br>";
            } else {
                $msg= "Error al subir el archivo <br>";
        }

        } else {
            $msg;
        }
        $_SESSION["mensaje"]=$msg;
        $this->loadArray();
    }

    //Metodo que carga las imagenes de la carpeta en el array
    public function loadArray()
    {   
        //Creamos el array
        $arrayImagenes=[];
        
        //Recojemos la carpeta donde se encuentran las imagenes
        $directorio = opendir("uploads"); //ruta actual
        while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
        {   
            //Si la imagen tiene este formato la agregaremos
            if (strrpos($archivo, ".jpg")||strrpos($archivo, ".jpeg"))//verificamos si es o no un directorio
            {

                $arrayImagenes[]="uploads/".$archivo; //de ser un directorio lo envolvemos entre corchetes
            }
        }

        require('view.php');
    }
    //Metodo que elimina la imagen del array
    public function deleteImage()
    {   
        //Mensaje que devolvemos
        $msg="";
        //Si nos mandan la imagen
        if(file_exists($_GET["clave"])){
            
            //Intentamos borrar la imagen
            if (!unlink($_GET["clave"])){
            
                $msg= "no se pudo borrar el archivo :".$_GET["clave"]."<br>";
            }else{
                $msg= "Imagen Borrada Correctamente <br>";
            }
            $_SESSION["mensaje"]=$msg;
        }

    }


}
